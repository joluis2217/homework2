package com.emedinaa.kotlinapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_detalle_libro.view.*
import kotlinx.android.synthetic.main.fila.view.*


class Adapter (private var LibrosList : List<Libros>,val itemAction : (item : Libros) -> Unit)  :
    RecyclerView.Adapter<Adapter.LibrosViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibrosViewHolder {

            val view = LayoutInflater.from(parent.context).inflate(R.layout.fila,parent,false)
        return LibrosViewHolder(view)
    }

    override fun getItemCount(): Int {
       return LibrosList.count()
    }

    override fun onBindViewHolder(holder: LibrosViewHolder, position: Int) {
            holder.bind(LibrosList[position])
    }

    fun update(nLibros : List<Libros>){
        this.LibrosList=nLibros
        notifyDataSetChanged()

    }


    inner class  LibrosViewHolder(private val view : View) : RecyclerView.ViewHolder(view){

        fun bind(entity : Libros){
            view.imgImagen.setImageResource(entity.imagen)
            view.tvNombre.text=entity.nombre
            view.tvDescripcion.text=entity.descripcion

            view.setOnClickListener {
                itemAction(entity)
            }


        }

    }

}