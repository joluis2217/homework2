package com.emedinaa.kotlinapp

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Libros(val id:Int,val nombre:String,val descripcion:String,val imagen:Int ) :
    Parcelable {
    override fun toString(): String {
        return "Libros(id=$id, nombre='$nombre', descripcion='$descripcion', image=$imagen)"
    }
}



