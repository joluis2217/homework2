package com.emedinaa.kotlinapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_detalle_libro.*
import kotlinx.android.synthetic.main.activity_main.*

class DetalleLibro : AppCompatActivity() {




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detalle_libro)
        populate()

    }


    private fun populate(){
        (intent?.extras?.getParcelable("ENTITY") as? Libros)?.let {
            tvDescripcionDet.text = it.descripcion
            tvNombreDet.text = it.nombre
            imgImagenDet.setImageResource(it.imagen)
        }


    }

}