package com.emedinaa.kotlinapp

object Data {

    fun getLibros() = listOf(

        Libros(
            1,"Historia de dos ciudades","Esta novela del escritor británico Charles Dickens ha vendido más de 200 millones " +
                    "de copias.",R.drawable.libros
        ),
        Libros(
            2,"7 hábitos de la gente altamente efectiva","Este libro de Stephen R. Covey es una de las referencias del mundo empresarial. " +
                    "En sus hojas se repasan las costumbres más destacadas de las personas que resultan altamente efectivas en su día a día.",R.drawable.libros
        ),
        Libros(
            3,"Lo que el viento se llevó","Este libro de Margaret Mitchell es uno de los más vendidos de la historia y fue llevado al cine debido " +
                    "a su gran éxito. La historia transcurre durante la Guerra de Secesión de Estados Unidos y narra la historia de amor .",R.drawable.libros
        ),
        Libros(
            4,"El nombre de la rosa","Esta magnífica novela del recientemente fallecido Umberto Eco mezcla tanto la novela gótica y policiaca como la " +
                    "crónica medieval, y se centra en las actividades detectivescas de Guillermo de Baskerville para aclarar los asesinatos.",R.drawable.libros
        ),
        Libros(
            5,"Guerra y Paz","Esta novela del escritor ruso León Tolstói es un clásico de la literatura universal. Su texto narra las vidas de los miembros de" +
                    " distintas familias aristocráticas de la Rusia de entre 1805 y 1815.",R.drawable.libros
        )





    )




}