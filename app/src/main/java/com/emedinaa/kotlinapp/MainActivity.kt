package com.emedinaa.kotlinapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val adapter : Adapter by lazy {

        Adapter(Data.getLibros(),onItemAction())


    }

    private fun EnviarDetalle(item: Libros) {
        val bundle = Bundle().apply {
            putParcelable("ENTITY", item)
        }
        val intent = Intent(this, DetalleLibro::class.java)
        intent.putExtras(bundle)
        startActivity(intent)
    }


    fun onItemAction() : ( libro : Libros) -> Unit{
        return {
            EnviarDetalle(it)
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        recyclerView.adapter=adapter
    }
}